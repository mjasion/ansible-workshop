# Ansible workshop
## Requirements 
* docker (1.12) - https://get.docker.com/
* docker-compose (>=1.8) - https://docs.docker.com/compose/install/
* ansible (2.2) - http://docs.ansible.com/ansible/intro_installation.html

And execute
```bash
cd docker/
./restart.sh
```

## Workshops
### 1
```bash
ansible-playbook -i hosts playbook.yml
ansible-playbook -i hosts playbook.yml --skip-tags=clean
ansible-playbook -i hosts playbook.yml --tags=clean
```

### 2, 3
```bash
cd docker/
./restart.sh
cd ../2_docker

ansible-playbook -i docker_hosts playbook.yml
ansible-playbook -i docker_hosts playbook.yml --skip-tags=clean
ansible-playbook -i docker_hosts playbook.yml --tags=clean
```

# 4
```bash
cd docker/
./restart.sh
cd ../4_docker

ansible-playbook -i docker_hosts playbook.yml
```
# 5 
konfiguracja nginx