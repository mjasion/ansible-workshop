#!/usr/bin/env bash

docker-compose up -d

# second instance
docker-compose scale ubuntu_ssh=2
